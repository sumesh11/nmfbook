% Example on a randomly generated matrix with about 50% missing entries
clear all; clc; close all; 
m = 100; 
n = 100; 
r = 5; 
U0 = randn(m,r); 
V0 = randn(n,r); 
M = U0*V0'; 
W = round(rand(m,n)); % ~50% missing entries, W is binary 

options.r = r; 
[U,V,e] = WLRA(M,W,options); 
% Relative residual error
fprintf('Relative error ||M-UV''||_F/||M||_F = %2.2f%%.\n', ... 
    100*norm(M - U*V','fro')/norm(M,'fro')); 
set(0, 'DefaultAxesFontSize', 18);
set(0, 'DefaultLineLineWidth', 2);
figure; 
semilogy(e); 
xlabel('Iterations'); 
ylabel('||M-UV''||_W / ||M||_W'); 
% This codes solves the weighted low-rank matrix approximation problem. 
% Given M (m x n), a nonnegative weight matrix W (m x n), and r or an 
% initial matrix U (mxr), it iteratively solves
%
% min_{U(mxr), V(nxr)}  ||M-UV^T||_W^2 
%                               + lambda (||U||_F^2+||V||_F^2), 
% 
% where ||M-UV^T||_W^2 = sum_{i,j} W(i,j) (M-UV^T)_{i,j}^2, 
% 
% using a block coordinate descent method (columns of U and V are the 
% blocks, like in HALS for NMF). 
% 
% It is possible to requires (U,V) >= 0 using options.nonneg = 1. 
% 
% ---Input---
%  M        : (m x n) matrix to factorize
%  W        : (m x n) nonnegative weight matrix
% *** options *** 
%  r        : factorization rank r (defaut = 1) 
%  nonneg   : if nonneg = 1: U>=0 and V>=0 (default=0)
%  U        : (m x r) initialization for factor U (defaut=rand)
% init      : if init == 1: greedy approach (default) 
%                init == 2: random init 
%                init == 3: SVD/NMF init (using 0's for the unknown entries) 
%  maxiter  : number of iterations (defaut=100) 
%  lambda   : penalization parameter (default=1e-6)
%
% ---Output---- 
% U*V approximates M
% e(i) = relative error at iteration i = ||M-UV'||_W/||M||_W

function [U,V,e] = WLRA(M,W,options)

if W == 0
    error('W should contain at least one positive entry');
end

[m,n] = size(M); 
if nargin <= 2
    options = [];
end
if ~isfield(options,'r') && ~isfield(options,'U')
    options.r = 1;
end
if ~isfield(options,'U')
    options.U = rand(m,options.r); 
else
    options.r = size(options.U,2);
end
if ~isfield(options,'nonneg')
    options.nonneg = 0;
end
if ~isfield(options,'init') 
    options.init = 1;
end
if ~isfield(options,'lambda')
    lambda = 1e-6;
else
    lambda = options.lambda; 
end
if options.init == 1 % Greedy
    % Greedy initialization
    U = options.U;
    R = M;
    for k = 1 : options.r
        Rw = W.*R;
        V(:,k) = (Rw'*U(:,k))./(W'*(U(:,k).^2)+lambda);
        if options.nonneg == 1
            V(:,k) = max(eps,V(:,k));
        end
        U(:,k) = (Rw*V(:,k))./(W*(V(:,k).^2)+lambda);
        if options.nonneg == 1
            U(:,k) = max(eps,U(:,k));
        end
        R = R - U(:,k)*V(:,k)';
    end
elseif options.init == 2
    U = options.U;
    V = rand(n,options.r);
    % Scaling
    normMnz = sqrt( sum(sum( (M.^2).*(W>0) ) ));
    normUVnz = sqrt( sum(sum( ((U*V').^2).*(W>0) ) ));
    V = 0.9*V/(normUVnz*normMnz+eps);
    % Scaling of rank-one factors
elseif options.init == 3
    if options.nonneg == 1
        [U,V] = FroNMF(M,options.r); 
        V = V';
    else
        [u,s,v] = svds(M,options.r); 
        U = u*sqrt(s); 
        V = v*sqrt(s); 
    end
end
[U,V] = scalingUV(U,V);
if ~isfield(options,'maxiter')
    options.maxiter = 100;
end

error0 = sum(sum( (M.^2).*W ) ); 
R = M - U*V'; 
e(1) = sqrt(sum(sum( (R.^2).*W ) ) / (error0+eps)); 
% Main loop
for i = 2 : options.maxiter
    R = M - U*V'; 
    for k = 1 : options.r
        R = R+U(:,k)*V(:,k)'; 
        Rw = R.*W;
        U(:,k) = (Rw*V(:,k))./(W*(V(:,k).^2)+lambda);
        if options.nonneg == 1
            U(:,k) = max(eps,U(:,k)); 
        end
        V(:,k) = (Rw'*U(:,k))./(W'*(U(:,k).^2)+lambda);
        if options.nonneg == 1
            V(:,k) = max(eps,V(:,k)); 
        end
        R = R-U(:,k)*V(:,k)';
    end
    [U,V] = scalingUV(U,V); 
    e(i) = sqrt(sum(sum( (R.^2).*W ) ) / (error0+eps)); 
end

% Scaling of columns of U and V to have ||U(:,k)|| = ||V(:,k)|| for all k
function [U,V] = scalingUV(U,V)
[m,r] = size(U); 
normU = sqrt((sum(U.^2)))+1e-16;
normV = sqrt((sum(V.^2)))+1e-16;
for k = 1 : r
    U(:,k) = U(:,k)/sqrt(normU(k))*sqrt(normV(k));
    V(:,k) = V(:,k)/sqrt(normV(k))*sqrt(normU(k));
end
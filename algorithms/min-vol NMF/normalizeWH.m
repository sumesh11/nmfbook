% Normalization depending on the NMF model 

function [W,H] = normalizeWH(W,H,sumtoone,X) 

if sumtoone == 1 % Normalize so that H^Te <= e
                 % entries in cols of H sum to at most 1
    Hn = SimplexProj( H );
    if norm(Hn - H) > 1e-3*norm(Hn); 
       H = Hn; 
       % reoptimize W, because this normalization is NOT w.l.o.g. 
       options.inneriter = 100; 
       options.H = W'; 
       W = nnls_PFGM(X',H',options); 
       W = W'; 
    end
    H = Hn; 
elseif sumtoone == 2 % Normalize so that He = e, 
                     % entries in rows of H sum to 1
    scalH = sum(H');
    H = diag( scalH.^(-1) )*H;
    W = W*diag( scalH );
elseif sumtoone == 3 % Normalize so that W^T e = e, 
                     % entries in cols of W sum to 1
    scalW = sum(W);
    H = diag( scalW )*H;
    W = W*diag( scalW.^(-1) );
end